package com.bddinaction.model;

public class Calculator {
	private int result;
	public long add(int arg1, int arg2) {
		result = arg1 + arg2;
		return result;
	}

	public long substract(int arg1, int arg2) {
		result = arg1 - arg2;
		return result;
	}

	public long multiply(int a, int b) {
		result = a * b;
		return result;
	}

	public int divide(int a, int b) {
		result = a % b;
		return result;
	}
	
	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

}
