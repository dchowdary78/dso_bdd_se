package com.bddinaction.model;

public interface FrequentFlyerStatusService {
    Status statusLevelFor(int statusPoints);
}
