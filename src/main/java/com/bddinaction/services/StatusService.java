package com.bddinaction.services;

import com.bddinaction.model.Status;

public interface StatusService {
    Status statusLevelFor(int statusPoints);
}
