package com.bddinaction.junit;


import org.junit.Test;

import com.bddinaction.model.FrequentFlyer;
import com.bddinaction.model.Status;

import static org.fest.assertions.Assertions.assertThat;

public class WhenRegisteringANewFrequentFlyerMember {

    @Test
    public void should_be_able_to_create_a_new_member() {
        FrequentFlyer member = FrequentFlyer.withFrequentFlyerNumber("123456789")
                .named("Jill", "Smith");

        assertThat(member.getFirstName()).isEqualTo("Jill");
        assertThat(member.getLastName()).isEqualTo("Smith");
        assertThat(member.getFrequentFlyerNumber()).isEqualTo("123456789");
    }

    @Test
    public void the_members_initial_status_should_be_bronze() {
        FrequentFlyer member = FrequentFlyer.withFrequentFlyerNumber("123456789")
                .named("Jill", "Smith");

        assertThat(member.getStatus()).isEqualTo(Status.Bronze);
    }
}
