package com.bddinaction.junit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import com.bddinaction.model.Calculator;

public class TestCalculator {
	private Calculator calculator;

	@Before
	public void setUp() {
		// Arrange
		 calculator = new Calculator();
	}

	@Test
	public void testAdd() {
		int a = 15;
		int b = 20;
		int expectedResult = 35;
		// Act
		long result = calculator.add(a, b);
		// Assert
		Assert.assertEquals(expectedResult, result);
	}

	@Test
	public void testSubtract() {
		int a = 25;
		int b = 20;
		int expectedResult = 5;
		long result = calculator.substract(a, b);
		Assert.assertEquals(expectedResult, result);
	}

	@Test
	public void testMultiply() {
		int a = 10;
		int b = 25;
		long expectedResult = 250;
		long result = calculator.multiply(a, b);
		Assert.assertEquals(expectedResult, result);
	}

	@Test
	public void testDivide() {
		int a = 56;
		int b = 10;
		int expectedResult = 6;
		int result = calculator.divide(a, b);
		Assert.assertEquals(expectedResult, result);
	}

}

