package com.bddinaction.cucumber.steps;


import com.bddinaction.model.FrequentFlyer;

public class FrequentFlyerHelper {
    private FrequentFlyer frequentFlyer;

    public void setFrequentFlyer(FrequentFlyer frequentFlyer) {
        this.frequentFlyer = frequentFlyer;
    }

    public FrequentFlyer getFrequentFlyer() {
        return frequentFlyer;
    }

}
