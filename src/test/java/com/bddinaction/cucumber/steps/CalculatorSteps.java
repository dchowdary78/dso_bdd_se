package com.bddinaction.cucumber.steps;


import com.bddinaction.model.Calculator;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;

public class CalculatorSteps {
	
	Calculator calculator=new Calculator();
	
	@Given("^I have a Caculator$")
	public void i_have_a_caculator() {

	}
	@When("^I add (\\d+) and (\\d+)$")
	public void i_add_and(int int1, int int2) {
		calculator.add(int1,int2);
	}
	@When("^I substract (.*) and (.*)$")
	public void i_substract_and(Integer int1, Integer int2) {
	   calculator.substract(int1,int2);
			   
	}
	@Then("^The result should be (.*)$")
	public void the_result_should_be(int int1) {
		assertEquals(calculator.getResult(),int1);
	}

	}



