package com.bddinaction.cucumber.steps;

import com.bddinaction.cucumber.steps.persona.FrequentFlyerPersona;
import com.bddinaction.model.Status;

import io.cucumber.java.en.Given;

public class TransferSteps {

    private final FrequentFlyerHelper frequentFlyerHelper;

    public TransferSteps() {
		this.frequentFlyerHelper = new FrequentFlyerHelper();
    	
    }
    
//    Both Login and earn extra points frm status given file wrote here
    public TransferSteps(FrequentFlyerHelper frequentFlyerHelper) {
        this.frequentFlyerHelper = new FrequentFlyerHelper();
//		this.frequentFlyerHelper = frequentFlyerHelper;
    }

    @Given("^(.*) is a Frequent Flyer member$")
    public void Joe_is_a_Frequent_Flyer_member(FrequentFlyerPersona frequentFlyerPersona) throws Throwable {
        frequentFlyerHelper.setFrequentFlyer(frequentFlyerPersona.getFrequentFlyer());
    }

 
}
