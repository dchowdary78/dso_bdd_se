package com.bddinaction.cucumber.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class FlightDetailsSteps {
    @Given("^I need to know the details of flight number (.*)$")
    public void I_need_to_know_the_details_of_flight_number(String flightNumber) throws Throwable {
    }

    @When("^I request the details about this flight$")
    public void I_request_the_details_about_this_flight() throws Throwable {
    }

    @Then("^I should receive the following:$")
    public void I_should_receive_the_following(DataTable flightDetails) throws Throwable {
    }
}
