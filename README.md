# CVS Health, DevSecOps Software Engineering BDD with Java Repository

## Behavior Driven Development

Behavior-Driven Development (BDD) is based on TDD, but TDD is focused on the internal processes of software and precision of code performance (unit tests), while BDD puts requirements and Business Value of software at the top of software priorities (acceptance tests). Meanwhile, acceptance tests are often modeled according to the User Stories and acceptance criteria. These tests are normally described in simple words so people from the outside of the IT industry (like shareholders, business analytics, QA engineers and Project Managers) understand them better.
#### Here’s how our development cycle looks like:
    1. Write a script on Gherkin
    2. Run the script and realize that nothing works properly
        2.1 Identify situations when this script must work
        2.2 Start checking these situations and realize that nothing functions well
        2.3 Identify and implement minimal functionality necessary for all the examples to come through the test
    3. Execute everything one more time. In case of new errors, go back to point 2.1
    4. The script works! Start creating a new script covering the major part of requirements

## Introduction

This repo contains a set of Java files that show how to use unit and BDD tests in tandem to test application code.

All unit-test were written using [Junit](https://junit.org/junit5/docs/current/user-guide/) and [Mockito](https://www.vogella.com/tutorials/Mockito/article.html). BDD test were written using [cucumber-js](https://cucumber.io).

There are Five BDD _features_  and the  _step definition_ files in this project:
## 
    1. Authentication
        1.1 logging_on.feature                                  :: LoginSteps.Java   
    2. Earning Points                                                                                                             
        2.1 earning_extra_points_from_status.feature            :: EarningPointsFromFlights.Java
        2.2 earning_points_from_purchases.feature               :: EarningPointsFromFlights.Java
    3. Viewing Flights 
        3.1 displaying_flight_details.feature                   :: FlightDetailsSteps.Java
    4. To Check the Status
        4.1 calculating_status_based_on_points.feature          :: EarningStatus.Java 
    5. Calculator 
        5.1 calc.feature                                        :: CalculatorSteps.Java 
## 
This is an example BDD test automation Project for Java 8 using the Cucumber framework. It contains a simple behavior scenarios that has My Flying High Website(Login,Earn&View Points,View Flights) and the Calculator project. 
To test a _feature_ the developer writes a _step definition_ file which is then run by _cucumber_ to test each scenario. The currespondent _step definition_ files are mentioned above.
#### Purpose 
This project was developed to demonstrate how to use Cucumber as a BDD test framework.

##### Folder Structure
## 
    1. src/main/java                       :: This contains an application's Java source files
    2. src/test/Java                       :: This contains Unit Tests and Step definitions files in Java      
        2.1 com.bddinaction.cucumber       :: Runner Files.
        2.2 com.bddinaction.cucumber.steps :: Step definition files
        2.3 com.bddinaction.junit          :: Unit test files 
    3. src/test/resources
        3.1 com.bddinaction.cucumber       :: Feature files specific resources used in this application.
## 

#### Developer Notes

**Software Stack Used**

 
    1. Cucumber         :: It's open-source software testing tool that supports BBD.It works with Gherkin because the Gherkin syntax structures plain text so that it can be read by the tool.Cucumber reads Gherkin tests and validates that the code performs as it should
    2. Gherkin          :: It is the Given-When-Then spec language.
    3. Maven            :: Build automation tool.
    4. Code Review Tools
        4.1 SonarLint   :: It lives only in the IDE. Its purpose is to give instantaneous feedback as you type your code. For this, it concentrates on what code you are adding or updating.
        4.2 Jacoco      :: Code coverage library for Java
        4.3 SonarCube   :: It is a central server that processes full analyses (triggered by the various SonarQube Scanners). Its purpose is to give a 360° vision of the quality of your code base. For this, it analyzes all the source lines of your project on a regular basis.
    5. IDE              :: Eclipse, Intellij, STS.
        
**About BDD**
Is my opinion that BDD is a great tool to use when BAs, developers, QA work together in the creation and validation of user stories because:

1. Plain english language can be used to describe scenarios and expectations
2. Scenarios can include sample data. The data in sample scenarios is used by developers while working on business logic for the application. Additionally, the same sample data is used by the developers when writting BDD tests to testing business logic.

**Challenges For The Organization**
BDD requires a different way to work and additional code. Teams interested in adapting BDD will benefit from more clear and consice user stories and acceptance criteria but, that comes at the cost of more test code.

The challenge is to recognize that even thought more time is spent in tests, the benefit of stable cleanner releases outweights the extra time need to write more tests.

The other challenge is the change in development culture. Teams need to be willing and able to recognize the benefit of BDD testing.

## Frequently Asked Questions 

#### What are “step definitions” in Cucumber?
Step definitions are the methods in the automation code that execute the steps. When a BDD framework runs a Gherkin scenario as a test, it “glues” each step to a step definition based on some sort of string matching.
#### How long should Gherkin scenarios be?
Scenarios should be bite-sized. Each scenario should focus on one individual behavior. There’s no hard rule, but I recommend single-digit step counts. 
#### Can BDD test frameworks be used for unit testing?
Yes, but I don’t recommend it. BDD frameworks shine for black-box feature testing. They’re a bit too verbose for code-level unit tests.
#### What is White Box Testing?
White box testing focuses on activities that require you to actually know the underlying source code of a program. Because the developer has written the code, he or she should understand it and have the ability to modify and test the actual code directly.
#### What is Black Box Testing?
Black box testing is the opposite. When performing black box testing activities, you are treating the program as a literal black box — meaning that you can't see into the actual source code of a feature, nor would you necessarily understand the underlying code of a particular feature. You act is if you have no idea how the program should work.
#### How many Gherkin scenarios should one story have?
There’s no hard rule, but I recommend no more than a handful of rules per story, and no more than a handful of examples per rule. If you do Example Mapping and feel overwhelmed by the number of cards for a story, then the story should probably be broken into smaller stories.
#### Can BDD be used with manual testing?
Yes! BDD is not merely an automation tool – it is a set of pragmatic practices to help teams develop better software. Gherkin scenarios are first and foremost behavior specs that help a team’s collaboration and accountability. They function secondarily as test cases that can be executed either manually or with automation.
#### How is BDD different from TDD (Test-Driven Development)?
BDD is an evolution of TDD. In TDD, developers (1) write unit tests and watch them fail, (2) develop the feature to make the tests pass, (3) refactor the code to make it stronger, and (4) repeat the cycle. In BDD, teams do this same loop with feature tests (a.k.a “acceptance” or “black-box” tests) as well as unit tests.
#### What is the difference between a scenario and a scenario outline?
A scenario is a procedure of Given-When-Then steps that covers one example for one behavior. If there are any parameters for steps, then a scenario has exactly one combination of possible inputs. A scenario outline is a Given-When-Then procedure that can have multiple examples of one behavior provided as a table of input combos. Each input row will run the same steps once, just with different parameter inputs.
#### In Gherkin, is it good or bad practice to have multiple Scenario Outlines with Examples tables in one feature file?

The short answer is yes, it is perfectly fine to have multiple Scenario Outlines within one feature file.

However, the unspoken concern with this question is the potential size of the feature file. If one Feature has multiple Scenario Outlines with large feature tables, then the feature file could become unreadable. Remember, Gherkin is a specification language, not a programming language. A feature file should look more like a meaningful behavior example than a giant wall of text or a low-level test script. Make sure to follow good Gherkin guidelines:
            1. Follow the Golden Gherkin Rule: Treat other readers as you would want to be treated.
            2. Follow the Cardinal Rule of BDD: One scenario, one behavior.
            3. Write declarative steps, not imperative ones.
            4. Try to limit the number of steps in each scenario to single digits.
            5. Use only a few rows and columns per example table.
#### ARE GHERKIN SCENARIOS WITH MULTIPLE WHEN-THEN PAIRS OKAY?
One of the dead giveaways to violations of the Cardinal Rule of BDD is when a Gherkin scenario has multiple When-Then pairs, like this:
#### 
    Feature: Yahoo Searching
    Scenario: Yahoo Image search shows pictures
            Given the user opens a web browser
            And the user navigates to "https://www.yahoo.com/"
            When the user enters "CVS" into the search bar
            Then links related to "CVS" are shown on the results page
            When the user clicks on the "Images" link at the top of the results page
            Then images related to "CVS" are shown on the results page
    
            A When-Then pair denotes a unique behavior. In this example, the behaviors of performing a search and changing the search to images 
            could and should clearly be separated into two scenarios, like this:
    Feature: Yahoo Searching
    Scenario: Search from the search bar
        Given a web browser is at the Yahoo home page
        When the user enters "CVS" into the search bar
        Then links related to "CVS" are shown on the results page
    Scenario: Image search
        Given Yahoo search results for "CVS" are shown
        When the user clicks on the "CVS" link at the top of the results page
        Then images related to "CVS" are shown on the results page
#### 
#### No Double-Quotes Around Step Parameters
How do you know if something is a step parameter? “Double quotes” make it easy. However, Gherkin does not enforce double quotes around parameters. It is merely by programmer’s convention, but it’s a really helpful convention indeed.
####No Tags
Tags make it super easy to filter scenarios at runtime. No tags? Good luck remembering long paths and names at runtime, or running related scenarios across different feature files together.
#### More Than 10 Steps per Scenario
Again, any longer is too much to comprehend. Scenarios should be short and sweet – they should concisely describe behavior. Too many steps means the scenario is too imperative or covers more than one behavior.
#### Multiple Behaviors per Scenario
Scenarios should not have multiple personality disorder: one scenario, one behavior. Don’t break the Cardinal Rule of BDD! So many people break this rule when they first start BDD because they are locked into procedure-driven thinking. Then, when tests fail, nobody knows exactly what behavior is the culprit. One scenario, one behavior.
#### Difference between Unit Testing vs BDD
The difference is that while JUnit aims at tests, Cucumber aims at collaboration with non-technical people. Non technical people will not understand what a unit test does. They will, however, be able to understand and validate an example written in Gherkin.







