#Author: Software Engineering DevSecOps
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Calculator
Feature: Caculator

	Background: 
 		Given I have a Caculator
 	@Add2Num	
  Scenario: Add Two numbers
#		Given I have a Caculator
    When I add 2 and 3
    Then The result should be 5
  @Sub2Num
  Scenario Outline: Substract two numbers
#		Given I have a Caculator
    When  I substract <value 1> and <value 2>
    Then The result should be <result>
    
    Examples:
    | value 1 	 | value 2 	| result |
    |  10        |   5      |  5     |
    |  20        |   4      |  16    |
    |  6         |   1      |  5     |
    |  18        |   25     |  -7    |
    
    
    

  
